package formasapp;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author daniel.conrado
 */
public class Tela extends JPanel {

    protected static final int TRIANGULO = 3;
    protected static final int RETANGULO = 2;
    protected static final int CIRCULO = 1;
    
    protected static final int FORMA_TIPO = 0;
    protected static final int FORMA_X = 1;
    protected static final int FORMA_Y = 2;
    protected static final int FORMA_LARGURA = 3;
    protected static final int FORMA_ALTURA = 4;
    
    private final List<int[]> formas = new LinkedList<>();
    
    /**
     * Constrói uma tela de fundo branco.
     * @return 
     */
    public static Tela fabricarTela() {
        Tela t = new Tela();
        t.setBackground(Color.WHITE);
        return t;
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        
        super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
        g.setColor(Color.BLACK);
        
        formas.forEach((forma) -> {
            final int tipo = forma[FORMA_TIPO];
            final int x = forma[FORMA_X];
            final int y = forma[FORMA_Y];
            final int largura = forma[FORMA_LARGURA];
            final int altura = forma[FORMA_ALTURA];
            
            switch (tipo) {
                // circulo
                case CIRCULO:
                    g.drawOval(x, y, largura, altura);
                    break;
                // retângulo
                case RETANGULO:
                    g.drawRect(x, y, largura, altura);
                    break;
                // triângulo
                case TRIANGULO:
                    /* Desenhar um caminho de três pontos:
                        Ponto 1: (x, y + altura)
                        Ponto 2: (x + (largura/2), y)
                        Ponto 3: (x + largura, y + altura)
                    */
                    g.drawPolygon(
                        new int[]{x,          x + (largura/2), x + largura},
                        new int[]{y + altura, y,               y + altura },
                    3);
                    break;
            }
        });
        
    }
    
    public void desenharCirculo(int x, int y, int largura, int altura) {
        formas.add(new int[]{CIRCULO, x, y, largura, altura});
        this.repaint();
    }
    
    public void desenharRetangulo(int x, int y, int largura, int altura) {
        formas.add(new int[]{RETANGULO, x, y, largura, altura});
        this.repaint();
    }
    
    public void desenharTriangulo(int x, int y, int largura, int altura) {
        formas.add(new int[]{TRIANGULO, x, y, largura, altura});
        this.repaint();
    }
}
